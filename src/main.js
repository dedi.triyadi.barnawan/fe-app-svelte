import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: 'broo:v6 siapp'
	}
});

export default app;
